-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Out-2019 às 21:48
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beatriz`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `courses`
--

CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(100) NOT NULL,
  `course_img` varchar(100) NOT NULL,
  `course_description` text NOT NULL,
  `course_duration` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `courses`
--

INSERT INTO `courses` (`course_id`, `course_name`, `course_img`, `course_description`, `course_duration`) VALUES
(4, 'Donnuts', '/public/images/courses/portfolio-1.jpg', 'Donnuts com cobertura de morango e personalizados. ', '0000-00-00'),
(5, 'Guloseimas Fini. ', '/public/images/courses/portfolio-2.jpg', 'Guloseimas de gelatina. ', '0000-00-00'),
(6, 'Oreo Ice Cream', '/public/images/courses/portfolio-3.jpg', 'Bolacha Oreo com sorvete. ', '0000-00-00'),
(7, 'Macaroons', '/public/images/courses/portfolio-4.jpg', 'Bolachas recheadas finas. ', '0000-00-00'),
(8, 'Sorvetes Waffles', '/public/images/courses/portfolio-5.jpg', 'Sorvetes com casca de waffles. ', '0000-00-00'),
(9, 'Churros', '/public/images/courses/portfolio-6.jpg', 'Churros recheados com doce de leite. ', '0000-00-00'),
(10, 'Alfajor', '/public/images/courses/portfolio-7.jpg', 'Alfajor importado. ', '0000-00-00'),
(11, 'Donnuts simples coloridos.', '/public/images/courses/portfolio-8.jpg', 'Donnuts com diversas coberturas (Brigadeiro, morango, doce de leite, tradicional, morango, blueberry). ', '0000-00-00'),
(12, 'Waffles. ', '/public/images/courses/portfolio-9.jpg', 'Waffles com coberturas doces. ', '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `team`
--

CREATE TABLE `team` (
  `member_id` int(100) NOT NULL,
  `member_name` varchar(100) NOT NULL,
  `member_photo` varchar(100) DEFAULT NULL,
  `member_description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `team`
--

INSERT INTO `team` (`member_id`, `member_name`, `member_photo`, `member_description`) VALUES
(2, 'Erick Jacquin', '/public/images/team/author-2.jpg', 'Chef francês, chegou ao Brasil em 1995 para comandar o restaurante Le Coq Hardy, que acabou rendendo a Jacquin o prêmio de chef do ano e ao estabelecimento o de melhor restaurante do Brasil. Hoje possui apenas um restaurante, o Tartar&Co, aclamado pelos críticos e pelos clientes e também atua como jurado no programa de culinária Masterchef Brasil.'),
(3, 'Paola Carosella ', '/public/images/team/author-3.jpg', 'Famosa por integrar a equipe de jurados do Masterchef Brasil junto com Henquique Fogaça e Erick Jacquin, Paola é argentina e cresceu rodeada de boa comida e um amplo contato com a natureza. Começou a carreira na cozinha depois de terminar o colegial, em Buenos Aires. Expandiu seus horizontes culinários quando viajou para Paris, California, Uruguai e Nova Iorque, cozinhando em renomados restaurante em todos esses países. Paolla tem dois restaurantes ativos, o Arturito e o La Guapa, ambos em São Paulo.'),
(4, 'Jamie Oliver', '/public/images/team/author-4.jpg', 'Conhecido por sua iniciativa de revolucionar a alimentação nas escolas, Jamie é britânico e preza pelo uso de alimentos orgânicos e frescos. Começou sua carreira aos 16 anos, em uma escola de culinária em Westminster. Hoje é dono do Fifteen, em Londres, e uma cadeia de restaurantes italianos mundo afora, chamados “James´s Italian”. São mais de cinquenta unidades do estabelecimento; uma, inclusive, foi aberta em São Paulo em abril de 2015.'),
(5, 'Gastón Acurio', '/public/images/team/author-6.jpg', 'O peruano comanda o restaurante Astrid y Gastón, em Lima, que foi considerado o melhor da América Latina em 2013, pela revista “Restaurant”. É conhecido por apresentar a edição de 2011 do Masterchef de seu país e seu ativismo social. É dono de duas escolas de culinária dedicadas à população carente peruana e tem cerca de 40 restaurantes pelo mundo.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_login` varchar(30) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `user_full_name` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `password_hash`, `user_full_name`, `user_email`) VALUES
(1, 'beatriz', '$2y$10$gkCpdfV8Zr2p.v1ap64g4uqj0JcXCZAerwB5gSfNBSk1sJbdcsR1C', 'BeatrizAlvisi', 'alvisibeatriz@hotmail.com'),
(3, 'professor', '$2y$10$xgN7oDyzo9FDa3Aem.Lu7uMrsYWReRKy4JG2jBv.uwA/oZb5pmGvy', 'Professor ', 'professor@gmail.com'),
(4, 'Paula', '$2y$10$PsC4agIE/OocFp0XbdVDT.yNyfukLH.a24p9ap8DqaeKrbFivfA22', 'Paula Martins', 'paulamartins@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `member_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
